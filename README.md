**Blox Bot**
***BloxBot*** is an Open-Source discord bot, powered by Discord.JS
*BloxBot* will have many features, including but not limited to:
-- **Game Stats**
--- Overwatch
--- *Minecraft*
---- Server Pinger
---- Hypixel Stats - Only with Hypixel API Key
--- Fortnite - Only with Fortnite ID
-- **Moderation**
--- Spam Protection
--- Anti-Bad words
--- Muting
--- Message reporting system
--- Admin Logs
--- Announcements
-- **Tools**
--- Translating
--- Calculator
--- Song Lyrics
--- Pasting System - Powered by BloxPaste